package com.webserver.request;

import com.webserver.exceptions.WebRequestException;
import com.webserver.helpers.HTTPHeader;
import com.webserver.request.requestLine.RequestLine;
import com.webserver.request.requestLine.RequestLineParser;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static com.webserver.logging.SimpleLogger.err;

public class RequestReader implements Closeable {

    private final InputStream in;
    private final int bufferSize;

    public RequestReader(InputStream in, int bufferSize) {
        this.in = in;
        this.bufferSize = bufferSize;
    }

    public void close() throws IOException {
        in.close();
    }

    public Request receiveRequest() throws IOException, WebRequestException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        Request.RequestBuilder builder = Request.RequestBuilder.newInstance();

        try {
            RequestLine requestLine = new RequestLineParser().read(readLine(reader));
            builder.requestLine(requestLine);
        } catch (IllegalArgumentException e) {
            throw new WebRequestException(e.getMessage(), e);
        }

        int contentLength = 0;
        String headerLine = readLine(reader);
        while(!headerLine.isEmpty()) {
            int delimiter = headerLine.indexOf(": ");
            String field = headerLine.substring(0, delimiter);
            String value = headerLine.substring(delimiter + 2);

            if (field.isEmpty() || value.isEmpty()) {
                throw new WebRequestException("Header could not be parsed");
            }

            builder.header(field, value);
            
            if (HTTPHeader.CONTENT_LENGTH.equals(field)) {
                contentLength = Integer.parseInt(value);
            }
            headerLine = readLine(reader);
        }

        if (contentLength > 0) {
            byte[] body = readBody(reader);
            builder.body(body);

            if (body.length < contentLength) {
                // if strict approach, we can consider to throw an Exception here
                err("It was not possible to load the whole request body. Only %s bytes were loaded.", body.length);
            }
        }

        return builder.build();
    }

    private String readLine(BufferedReader reader) throws IOException {
        String line = reader.readLine();
        if (line == null) 
            throw new IOException("Invalid data from the input stream.");

        return line;
    }

    /** Attention! For demo only.
     * This method reads the bytes stream into a char array, and then convert back to bytes.
     * It may be prone to errors and corner cases.
     * */
    private byte[] readBody(BufferedReader reader) throws IOException {
        StringBuffer out = new StringBuffer();
        char[] buffer = new char[bufferSize];
        while (reader.ready()) {
            int read = reader.read(buffer);
            if (read == -1)
                break;
            out.append(buffer, 0, read);
        }
        return out.toString().getBytes(StandardCharsets.UTF_8);
    }
}