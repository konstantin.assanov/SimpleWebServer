package com.webserver.request;

import com.webserver.controller.IController;
import com.webserver.helpers.HTTPHeader;
import com.webserver.helpers.ProtocolVersion;
import com.webserver.request.requestLine.RequestLine;
import com.webserver.response.Response;

import java.util.HashMap;
import java.util.Map;

public class Request {
    private final RequestLine requestLine;
    private final Map<String, String> headers = new HashMap<String, String>();
    private final byte[] body;

    private Request(RequestLine requestLine, Map<String, String> headers, byte[] body) {
        this.requestLine = requestLine;
        this.headers.putAll(headers);
        this.body = body;
    }

    public RequestLine getRequestLine() {
        return requestLine;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public byte[] getBody() {
        return body;
    }

    public Response processWith(IController controller) {
        return controller.processRequest(this);
    }

    public boolean isToKeepAlive() {
        boolean isConnectionDefined = headers.containsKey(HTTPHeader.CONNECTION);
        String connection = "";
        if (isConnectionDefined)
            connection = headers.get(HTTPHeader.CONNECTION);

        if (requestLine.getProtocolVersion() == ProtocolVersion.HTTP10
                && isConnectionDefined
                && HTTPHeader.CONN_KEEP_ALIVE.equals(connection))
            return true;

        if (requestLine.getProtocolVersion() == ProtocolVersion.HTTP11
                && (!isConnectionDefined
                || !HTTPHeader.CONN_CLOSE.equals(connection)))
            return true;

        return false;
    }

    public RequestLine.Method getMethod() {
        return requestLine.getMethod();
    }

    public String getPath() {
        return requestLine.getUri().getPath();
    }

    public ProtocolVersion version() {
        return requestLine.getProtocolVersion();
    }

    public static class RequestBuilder {
        private RequestLine requestLine;
        private Map<String, String> headers = new HashMap<String, String>();
        private byte[] body;

        private RequestBuilder() { }

        public static RequestBuilder newInstance() {
            return new RequestBuilder();
        }

        public RequestBuilder requestLine(RequestLine requestLine) {
            this.requestLine = requestLine;
            return this;
        }

        public RequestBuilder header(String field, String value) {
            headers.put(field, value);
            return this;
        }

        public RequestBuilder body(byte[] body) {
            this.body = body;
            return this;
        }

        public Request build() {
            return new Request(requestLine, headers, body);
        }
    }
}