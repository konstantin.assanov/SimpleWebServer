package com.webserver.request.requestLine;

import com.webserver.helpers.ProtocolVersion;

import java.net.URI;

public class RequestLineParser {
    public RequestLine read(String requestLine) throws IllegalArgumentException {
        // check 3 parts existence
        String[] parts = requestLine.split(" ");
        if (parts.length < 3)
            throw new IllegalArgumentException("Invalid HTTP request line.");

        RequestLine.Method method;
        try {
            method = RequestLine.Method.valueOf(parts[0].trim().toUpperCase());
        } catch (IllegalArgumentException iae) {
            throw new IllegalArgumentException("HTTP request line has invalid method.");
        }

        URI uri;
        try {
            uri = URI.create(parts[1].trim());
        } catch(IllegalArgumentException iae) {
            throw new IllegalArgumentException("HTTP request line has invalid uri.");
        }

        ProtocolVersion protocolVersion = ProtocolVersion.byName(parts[2].trim());
        if (protocolVersion == null)
            throw new IllegalArgumentException("HTTP request line has invalid protocol version");

        return new RequestLine(method, uri, protocolVersion);
    }
}
