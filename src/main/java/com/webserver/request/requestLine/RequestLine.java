package com.webserver.request.requestLine;

import com.webserver.helpers.ProtocolVersion;

import java.net.URI;

public class RequestLine {
    public enum Method {
        HEAD,
        GET,
        POST,
        PUT,
        DELETE
    }

    private final Method method;
    private final URI uri;
    private final ProtocolVersion protocolVersion;

    public RequestLine(Method method, URI uri, ProtocolVersion protocolVersion) {
        this.method = method;
        this.uri = uri;
        this.protocolVersion = protocolVersion;
    }

    public Method getMethod() {
        return method;
    }

    public URI getUri() {
        return uri;
    }

    public ProtocolVersion getProtocolVersion() {
        return protocolVersion;
    }
}
