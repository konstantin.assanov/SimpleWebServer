package com.webserver.html;

public class HtmlTemplate {
    public static final String htmlSimple = new StringBuffer()
        .append("<!DOCTYPE html>\n")
        .append("<html lang=\"en\">\n")
        .append("  <head>\n")
        .append("    <meta charset=\"utf-8\">\n")
        .append("    <title>%s</title>\n")
        .append("  </head>\n")
        .append("  <body>%s</body>\n")
        .append("</html>")
        .toString();

    public static String simple(String msg) {
        return String.format(htmlSimple, msg, msg);
    }
}
