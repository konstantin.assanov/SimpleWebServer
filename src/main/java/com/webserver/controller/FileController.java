package com.webserver.controller;

import com.webserver.config.FileServerConfig;
import com.webserver.config.ServerConfig;
import com.webserver.exceptions.FailedResponseData;
import com.webserver.exceptions.WebProcessException;
import com.webserver.helpers.FileExtension;
import com.webserver.helpers.MediaType;
import com.webserver.request.Request;
import com.webserver.response.Response;
import com.webserver.response.StatusLine;

import java.io.*;

class FileController extends AbstractController {

    private final ServerConfig configGeneral;
    private final FileServerConfig config;

    FileController(ServerConfig config) {
        super(config.fileServerConfig.serverName);
        this.configGeneral = config;
        this.config = config.fileServerConfig;
    }

    private static String getExtension(String filename) {
        int dotIndex = filename.lastIndexOf(".");
        if (dotIndex == -1)
            return null;

        return filename.substring(dotIndex + 1);
    }

    private boolean validatePath(String path) {
        final String[] pathTokens = path.split(File.separator);
        for (String token : pathTokens)
            if ("..".equals(token))
                return false;

        return true;
    }

    private File findFile(String path) {
        File temp = new File(config.root, path);
        if (!temp.exists() || temp.isHidden()) {
            return null;
        }

        return temp;
    }

    private File findFileOrDefault(String path) {
        File temp = findFile(path);
        if (temp == null)
            return null;

        if (temp.isDirectory()) {
           return findFile(path + File.separator + config.defaultFilename);
        }

        return temp;
    }

    private class FileBodyWriter implements Response.BodyWriter {
        private final File file;
        FileBodyWriter(File file) {
            this.file = file;
        }

        public void writeTo(OutputStream out) throws IOException {
            try (InputStream is = new FileInputStream(file)) {
                byte[] buffer = new byte[configGeneral.OUTPUT_BUFFERSIZE];

                while (is.available() > 0) {
                    int read = is.read(buffer);
                    out.write(buffer, 0, read);
                }
            }
        }
    }

    Response head(String path, Request request) throws UnsupportedOperationException {
        return headOrGet(path, request, false);
    }

    Response get(String path, Request request) throws UnsupportedOperationException {
        return headOrGet(path, request, true);
    }

    private Response headOrGet(String path, Request request, boolean withBody) throws UnsupportedOperationException {
        if (!validatePath(path))
            throw new WebProcessException("Invalid path at request URI",
                    new FailedResponseData(StatusLine.Status.BAD_REQUEST,
                            request.isToKeepAlive(),
                            request.version(),
                            withBody));

        final File f = findFileOrDefault(path);
        if (f == null)
            throw new WebProcessException("File not found",
                    new FailedResponseData(StatusLine.Status.NOT_FOUND,
                            request.isToKeepAlive(),
                            request.version(),
                            withBody));

        final MediaType mediaType = FileExtension.byExtension(getExtension(f.getName()));
        if (config.filesWithKnownExtensionsOnly && mediaType == MediaType.UNKNOWN_FILE_TYPE)
            throw new WebProcessException("The requested file has unknown media type",
                    new FailedResponseData(StatusLine.Status.UNSUPPORTED_MEDIA_TYPE,
                            request.isToKeepAlive(),
                            request.version(),
                            withBody));

        return Response.ResponseBuilder.newInstance()
                .OK()
                .server(config.serverName)
                .mediaType(mediaType, f.getName())
                .keepAlive(request.isToKeepAlive(), request.version())
                .contentLength(f.length())
                .body(withBody ? new FileBodyWriter(f) : null)
                .build();
    }
}