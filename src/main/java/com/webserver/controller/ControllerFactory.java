package com.webserver.controller;

import com.webserver.config.ServerConfig;

public class ControllerFactory {
    private final ServerConfig config;
    public ControllerFactory(ServerConfig config) {
        this.config = config;
    }

    public IController newController() {
        AbstractController controller = null;
        switch (config.controllerType) {
            case echo:
                controller = new EchoController(config.echoServerName);
                break;
            case file:
                controller = new FileController(config);
                break;
        }
        return controller;
    }
}
