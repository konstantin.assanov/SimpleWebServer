package com.webserver.controller;

import com.webserver.request.Request;
import com.webserver.response.Response;

public interface IController {
    Response processRequest(Request request);
}
