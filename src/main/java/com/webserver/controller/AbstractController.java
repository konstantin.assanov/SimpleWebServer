package com.webserver.controller;

import com.webserver.exceptions.FailedResponseData;
import com.webserver.helpers.ProtocolVersion;
import com.webserver.request.Request;
import com.webserver.response.Response;
import com.webserver.response.StatusLine;

import static com.webserver.logging.SimpleLogger.err;

class AbstractController implements IController {

    protected final String serverName;

    AbstractController(String serverName) {
        this.serverName = serverName;
    }

    public final Response processRequest(Request request) {
        Response response;

        try {
            String path = request.getPath();

            switch (request.getMethod()) {
                case HEAD:
                    response = head(path, request);
                    break;
                case GET:
                    response = get(path, request);
                    break;
                case PUT:
                    response = put(path, request);
                    break;
                case POST:
                    response = post(path, request);
                    break;
                case DELETE:
                    response = delete(path, request);
                    break;
                default:
                    err("Unknown request method.");
                    response = NotImplemented(request.isToKeepAlive(), request.version());
            }
        } catch (UnsupportedOperationException uoe) {
            err(uoe.getMessage());
            response = NotImplemented(request.isToKeepAlive(), request.version());
        }

        return response;
    }

    Response head(String path, Request request) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("The method HEAD is not supported");
    }

    Response get(String path, Request request) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("The method GET is not supported");
    }

    Response put(String path, Request request) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("The method PUT is not supported");
    }

    Response post(String path, Request request) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("The method POST is not supported");
    }

    Response delete(String path, Request request) throws UnsupportedOperationException{
        throw new UnsupportedOperationException("The method DELETE is not supported");
    }

    private Response NotImplemented(boolean keepAlive, ProtocolVersion version) {
        return new FailedResponseData(
                StatusLine.Status.NOT_IMPLEMENTED,
                keepAlive,
                version,
                true)
                .buildResponse(serverName);
    }
}