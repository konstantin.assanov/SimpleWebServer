package com.webserver.controller;

import com.webserver.helpers.MediaType;
import com.webserver.html.HtmlTemplate;
import com.webserver.request.Request;
import com.webserver.response.Response;

class EchoController extends AbstractController {

    EchoController(String serverName) {
        super(serverName);
    }

    Response head(String path, Request request) throws UnsupportedOperationException {
        return headOrGet(path, request, false);
    }

    Response get(String path, Request request) throws UnsupportedOperationException {
        return headOrGet(path, request, true);
    }

    private Response headOrGet(String path, Request request, boolean withBody) throws UnsupportedOperationException {
        byte[] content = withBody ?
                HtmlTemplate.simple(String.format("Path: %s Query: %s",
                        path, request.getRequestLine().getUri().getQuery())).getBytes() : null;
        return Response.ResponseBuilder.newInstance()
                .OK()
                .server(serverName)
                .mediaType(MediaType.TEXT_HTML, null)
                .keepAlive(request.isToKeepAlive(), request.version())
                .contentLength((content != null) ? content.length : 0)
                .body(content)
                .build();
    }
}
