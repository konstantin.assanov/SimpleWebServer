package com.webserver.response;

import com.webserver.helpers.ProtocolVersion;

public class StatusLine {
    public enum Status {
        OK(200, "OK"),
        BAD_REQUEST(400, "Bad Request"),
        UNAUTHORIZED(401, "Unauthorized"),
        FORBIDDEN(403, "Forbidden"),
        NOT_FOUND(404, "Not Found"),
        REQUEST_TIMEOUT(408, "Request Timeout"),
        REQUEST_URI_TOO_LONG(414, "Request-URI Too Long"),
        UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type"),

        NOT_IMPLEMENTED(501, "Not Implemented");

        private final int id;
        private final String message;
        private final String idAndMessage;

        Status(int id, String message) {
            this.id = id;
            this.message = message;
            this.idAndMessage = String.format(" %s %s", id, message);
        }

        public int getStatusId() {
            return id;
        }

        public String getMessage() {
            return message;
        }

        public String getIdAndMessage() {
            return idAndMessage;
        }
    }

    public static String build(Status status) {
        return ProtocolVersion.HTTP11.getName() + status.getIdAndMessage();
    }
}
