package com.webserver.response;

import com.webserver.helpers.HTTPHeader;
import com.webserver.helpers.MediaType;
import com.webserver.helpers.ProtocolVersion;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class Response {

    public interface BodyWriter {
        void writeTo(OutputStream out) throws IOException;
    }

    static private class DefaultBodyWriter implements BodyWriter {
        private final byte[] body;
        private DefaultBodyWriter(byte[] body) {
            this.body = body;
        }
        public void writeTo(OutputStream out) throws IOException {
            if (body != null)
                out.write(body);
        }
    }

    public interface ResponseExporter {
        void write(Response response) throws IOException;
    }

    private final StatusLine.Status status;
    private final Map<String, String> headers = new HashMap<>();
    private final BodyWriter bodyWriter;
    private final boolean keepAlive;

    private Response(StatusLine.Status status, Map<String, String> headers, BodyWriter bodyWriter, boolean keepAlive) {
        this.status = status;
        this.headers.putAll(headers);
        this.bodyWriter = bodyWriter;
        this.keepAlive = keepAlive;
    }

    public StatusLine.Status getStatus() {
        return status;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public BodyWriter writer() {
        return bodyWriter;
    }

    public Response sendResponseWith(ResponseExporter exporter) throws IOException {
        exporter.write(this);
        return this;
    }

    public boolean isKeepAlive() {
        return keepAlive;
    }

    public static class ResponseBuilder {
        private StatusLine.Status status;
        private Map<String, String> headers = new HashMap<>();
        private BodyWriter bodyWriter;
        private boolean keepAlive = false;

        private ResponseBuilder() { }

        public static ResponseBuilder newInstance() {
            return new ResponseBuilder();
        }

        public ResponseBuilder status(StatusLine.Status status) {
            this.status = status;
            return this;
        }

        public ResponseBuilder OK() {
            this.status = StatusLine.Status.OK;
            return this;
        }

        public ResponseBuilder NotFound() {
            this.status = StatusLine.Status.NOT_FOUND;
            return this;
        }

        public ResponseBuilder BadRequest() {
            this.status = StatusLine.Status.BAD_REQUEST;
            return this;
        }

        public ResponseBuilder NotImplemented() {
            this.status = StatusLine.Status.NOT_IMPLEMENTED;
            return this;
        }

        public ResponseBuilder header(String field, String value) {
            headers.put(field, value);
            return this;
        }

        public ResponseBuilder server(String server) {
            headers.put(HTTPHeader.SERVER, server);
            return this;
        }

        public ResponseBuilder mediaType(MediaType mediaType, String filename) {
            headers.put(HTTPHeader.CONTENT_TYPE, mediaType.getName());
            if (mediaType == MediaType.UNKNOWN_FILE_TYPE) {
                headers.put(HTTPHeader.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"");
            }
            return this;
        }

        public ResponseBuilder contentLength(long length) {
            headers.put(HTTPHeader.CONTENT_LENGTH, Long.toString(length));
            return this;
        }

        public ResponseBuilder keepAlive(boolean keepAlive, ProtocolVersion version) {
            this.keepAlive = keepAlive;
            switch (version) {
                case HTTP10:
                    if (keepAlive)
                        headers.put(HTTPHeader.CONNECTION, HTTPHeader.CONN_KEEP_ALIVE);
                    break;
                case HTTP11:
                    if (!keepAlive)
                        headers.put(HTTPHeader.CONNECTION, HTTPHeader.CONN_CLOSE);
                    break;
            }

            return this;
        }

        public ResponseBuilder body(byte[] body) {
            this.bodyWriter = new DefaultBodyWriter(body);
            return this;
        }

        public ResponseBuilder body(BodyWriter bodyWriter) {
            this.bodyWriter = bodyWriter;
            return this;
        }

        public Response build() {
            return new Response(status, headers, bodyWriter, keepAlive);
        }
    }
}
