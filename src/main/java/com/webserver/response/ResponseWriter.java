package com.webserver.response;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Map;

public class ResponseWriter implements Response.ResponseExporter, Closeable {

    private final OutputStream out;

    public ResponseWriter(OutputStream out) {
        this.out = out;
    }

    public void close() throws IOException {
        out.close();
    }

    public void write(Response response) throws IOException {
        final PrintWriter writer = new PrintWriter(out);

        writer.println(StatusLine.build(response.getStatus()));

        final Map<String, String> headers = response.getHeaders();
        for (String key : headers.keySet()) {
            writer.println(key + ": " + headers.get(key));
        }
        writer.println();
        writer.flush();

        if (response.writer() != null)
            response.writer().writeTo(out);
    }
}