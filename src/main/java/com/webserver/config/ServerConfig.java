package com.webserver.config;

import com.webserver.controller.ControllerType;

import java.util.ArrayList;
import java.util.List;

public class ServerConfig {
    /* server listening port */
    public final int port = 8080;
    /* size of buffer used for reading the request body */
    public final int INPUT_BUFFERSIZE = 1024;
    /* size of buffer used to reading the file */
    public final int OUTPUT_BUFFERSIZE = 65536;
    /* nb of threads at fixed threads pool */
    public final int nbThreads = 10;
    /* keep-alive timeout */
    public final int readTimeout = 20000;

    /* type of controller to use */
    public final ControllerType controllerType = ControllerType.file; //echo;
    /* name used for 'server' header in case of Echo Server */
    public final String echoServerName = "Echo Server";
    /* configuration for file controller */
    public final FileServerConfig fileServerConfig = new FileServerConfig();

    public String serverName() {
        switch (controllerType) {
            case file:
                return fileServerConfig.serverName;
            case echo:
                return echoServerName;
            default:
                return "Unidentified Server";
        }
    }

    public List<String> validate() {
        List<String> errors = new ArrayList<String>();
        if (port < 1)
            errors.add("Port is not defined correctly.");

        if (INPUT_BUFFERSIZE < 1024)
            errors.add("Input Buffer Size is too small");

        if (OUTPUT_BUFFERSIZE < 1024)
            errors.add("Output Buffer Size is too small");

        switch (controllerType) {
            case file:
                errors.addAll(fileServerConfig.validate());
                break;
        }

        return errors;
    }
}
