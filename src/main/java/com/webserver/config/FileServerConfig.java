package com.webserver.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileServerConfig {
    /* name used for 'server' header */
    public final String serverName = "Simple Server";
    /* files with only known extensions to be served if true */
    public final boolean filesWithKnownExtensionsOnly = true;
    /* root directory for file resources */
    public final String root = "/example";
    /* filename to be used if undefined at request */
    public final String defaultFilename = "index.html";

    public List<String> validate() {
        final List<String> errors = new ArrayList<>();

        if (serverName == null || serverName.isEmpty())
            errors.add("Server name has to be defined");

        File rootDir = new File(root);
        if (!rootDir.exists() || !rootDir.isDirectory())
            errors.add("Root directory is not valid");

        if (defaultFilename != null && defaultFilename.contains(".."))
            errors.add("Default file name must not contain '..' or '/'");

        return errors;
    }
}
