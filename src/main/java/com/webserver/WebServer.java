package com.webserver;

import com.webserver.config.ServerConfig;
import com.webserver.controller.ControllerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.webserver.logging.SimpleLogger.err;
import static com.webserver.logging.SimpleLogger.info;

public class WebServer implements Runnable {
    private final ServerConfig config;

    public WebServer(ServerConfig config) {
        this.config = config;
    }

    public void run() {
        ControllerFactory factory = new ControllerFactory(config);
        ExecutorService threadPool = Executors.newFixedThreadPool(config.nbThreads);

        info("Webserver starting up on port %s.", config.port);
        info("Waiting for connection...");

        int threadId = 0;
        try (ServerSocket server = new ServerSocket(config.port);) {
            Socket client;
            while((client = server.accept()) != null) {
                threadPool.execute(new RequestProcessor(config, ++threadId, factory, client));
            }
        } catch (IOException e) {
            err(e.getMessage());
        } finally {
            threadPool.shutdown();
            info("Webserver stopped.") ;
        }
    }

    public static void main(String args[]) {
        ServerConfig config = new ServerConfig();
        List<String> errors = config.validate();
        if (errors.size() > 0) {
            System.out.println("Server could not be launched because of the wrong configuration:");
            for (String error : errors) {
                System.out.println("\t- " + error);
            }
            return;
        }

        new Thread(new WebServer(config)).start();
    }
}
