package com.webserver.helpers;

import static com.webserver.helpers.MediaType.*;

public enum FileExtension {
    css(TEXT_CSS),
    csv(TEXT_CSV),
    htm(TEXT_HTML), html(TEXT_HTML),
    js(TEXT_JS),mjs(TEXT_JS),
    txt(TEXT_PLAIN),

    bmp(IMAGE_BMP),
    gif(IMAGE_GIF),
    ico(IMAGE_ICO),
    jpeg(IMAGE_JPEG), jpg(IMAGE_JPEG),
    png(IMAGE_PNG),
    svg(IMAGE_GIF),
    tif(IMAGE_TIFF), tiff(IMAGE_TIFF),

    json(JSON),

    pdf(PDF),
    rtf(RTF);

    private final MediaType mediaType;

    FileExtension(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public static MediaType byExtension(String ext) {
        MediaType type;
        try {
            FileExtension extension = FileExtension.valueOf(ext.toLowerCase());
            type = extension.getMediaType();
        } catch (IllegalArgumentException|NullPointerException e) {
            type = MediaType.UNKNOWN_FILE_TYPE;
        }
        return type;
    }
}