package com.webserver.helpers;

public class HTTPHeader {
    public static final String SERVER = "Server";

    public static final String CONNECTION = "Connection";

    public static final String CONTENT_LENGTH = "Content-Length";

    public static final String CONTENT_TYPE = "Content-Type";

    public static final String CONTENT_DISPOSITION = "Content-Disposition";

    // standard values for 'Connection' header

    public static final String CONN_KEEP_ALIVE = "keep-alive";

    public static final String CONN_CLOSE = "close";
}
