package com.webserver.helpers;

import java.util.HashMap;
import java.util.Map;

public enum MediaType {
    TEXT_CSS("text/css"),
    TEXT_CSV("text/csv"),
    TEXT_HTML("text/html"),
    TEXT_JS("text/javascript"),
    TEXT_PLAIN("text/plain"),

    IMAGE_BMP("image/bmp"),
    IMAGE_GIF("image/gif"),
    IMAGE_ICO("image/vnd.microsoft.icon"),
    IMAGE_JPEG("image/jpeg"),
    IMAGE_PNG("image/png"),
    IMAGE_SVG("image/svg+xml"),
    IMAGE_TIFF("image/tiff"),

    JSON("application/json"),

    PDF("application/pdf"),
    RTF("application/rtf"),

    UNKNOWN_FILE_TYPE("application/octet-stream");

    private static final Map<String, MediaType> BY_NAME = new HashMap<String, MediaType>();

    static {
        for (MediaType p: values()) {
            BY_NAME.put(p.name, p);
        }
    }

    private final String name;

    MediaType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static MediaType byName(String name) {
        return BY_NAME.get(name);
    }
}