package com.webserver.helpers;

import java.util.HashMap;
import java.util.Map;

public enum ProtocolVersion {
    HTTP10("HTTP/1.0"),
    HTTP11("HTTP/1.1"),
    HTTP20("HTTP/2.0"),
    HTTP30("HTTP/3.0");

    private static final Map<String, ProtocolVersion> BY_NAME = new HashMap<String, ProtocolVersion>();

    static {
        for (ProtocolVersion p: values()) {
            BY_NAME.put(p.name, p);
        }
    }

    private final String name;

    ProtocolVersion(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static ProtocolVersion byName(String name) {
        return BY_NAME.get(name);
    }
}
