package com.webserver;

import com.webserver.config.ServerConfig;
import com.webserver.controller.ControllerFactory;
import com.webserver.controller.IController;
import com.webserver.exceptions.WebProcessException;
import com.webserver.exceptions.WebRequestException;
import com.webserver.request.RequestReader;
import com.webserver.response.ResponseWriter;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import static com.webserver.logging.SimpleLogger.*;

public class RequestProcessor implements Runnable {
    private final ServerConfig config;
    private final int threadId;
    private final ControllerFactory factory;
    private final Socket remote;

    RequestProcessor(ServerConfig config, int threadId, ControllerFactory factory, Socket remote) {
        this.config = config;
        this.threadId = threadId;
        this.factory = factory;
        this.remote = remote;
    }

    public void run() {
        infoThread(threadId, "THREAD STARTED. New connection from " + remote.getRemoteSocketAddress().toString());

        try {
            remote.setSoTimeout(config.readTimeout);
        } catch (SocketException se) {
            errThread(threadId, "Could not set timeout for the socket. %s", se.getMessage());
            try {
                remote.close();
            } catch(IOException ioe) {
                errThread(threadId, "Could not close the socket. %s", ioe.getMessage());
            }
            return;
        }

        int requestsServed = 0;
        try (final RequestReader requestReader = new RequestReader(remote.getInputStream(), config.INPUT_BUFFERSIZE);
             final ResponseWriter responseWriter = new ResponseWriter(remote.getOutputStream())) {
            IController controller = factory.newController();

            boolean keepAlive = true;
            while (keepAlive) {
                try {
                    keepAlive = requestReader
                            .receiveRequest()
                            .processWith(controller)
                            .sendResponseWith(responseWriter)
                            .isKeepAlive();
                } catch (WebRequestException e) {
                    errThread(threadId, "Bad request error: %s.", e.getMessage());
                    e.buildResponse(config.serverName())
                            .sendResponseWith(responseWriter);

                } catch (WebProcessException e) {
                    keepAlive = e.failedResponse()
                            .buildResponse(config.serverName())
                            .sendResponseWith(responseWriter)
                            .isKeepAlive();

                }
                infoThread(threadId, "Request #%s served. Keep Alive: %s.", (++requestsServed), keepAlive);
            }
        } catch (SocketTimeoutException e) {
            infoThread(threadId, "Expired keep-alive session.");
        } catch (IOException e) {
            errThread(threadId, "I/O exception: %s", e.getMessage());
        } catch (Exception e) {
            errThread(threadId, "Exception: %s", e.getMessage());
        }
        // input stream, output stream, and socket are closed automatically.

        infoThread(threadId, "Socket closed: (%s) of %s.",
                remote.isClosed(), remote.getRemoteSocketAddress());
        infoThread(threadId, "THREAD ENDED. Total %s requests served.", requestsServed);
    }
}