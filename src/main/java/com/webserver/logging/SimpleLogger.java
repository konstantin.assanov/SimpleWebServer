package com.webserver.logging;

public class SimpleLogger {
    public static void infoThread(int thread, String msg, Object... args) {
        log(String.format("info : (%s) %s", thread, msg), args);
    }
    public static void errThread(int thread, String msg, Object... args) {
        log(String.format("error: (%s) %s", thread, msg), args);
    }
    public static void info(String msg, Object... args) {
        log("info : " + msg, args);
    }
    public static void err(String msg, Object... args) {
        log("error: " + msg, args);
    }
    private static void log(String msg, Object... args) {
        System.out.printf(msg+"%n", args);
    }
}