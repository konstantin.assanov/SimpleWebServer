package com.webserver.exceptions;

import com.webserver.helpers.MediaType;
import com.webserver.helpers.ProtocolVersion;
import com.webserver.response.Response;
import com.webserver.response.StatusLine;

public class WebRequestException extends RuntimeException {
    public WebRequestException(String errorMessage,
                               Throwable err) {
        super(errorMessage, err);
    }

    public WebRequestException(String errorMessage) {
        super(errorMessage, null);
    }

    public Response buildResponse(String serverName) {
        String html = StatusLine.Status.BAD_REQUEST.getIdAndMessage();
        return Response.ResponseBuilder.newInstance()
                .BadRequest()
                .server(serverName)
                .mediaType(MediaType.TEXT_HTML, null)
                .keepAlive(true, ProtocolVersion.HTTP11)
                .contentLength(html.length())
                .body(html.getBytes())
                .build();
    }

}
