package com.webserver.exceptions;

import com.webserver.helpers.MediaType;
import com.webserver.helpers.ProtocolVersion;
import com.webserver.response.Response;
import com.webserver.response.StatusLine;

public class FailedResponseData {
    private final StatusLine.Status status;
    private final boolean keepAlive;
    private final ProtocolVersion version;
    private final boolean withBody;

    public FailedResponseData(StatusLine.Status status,
                               boolean keepAlive,
                               ProtocolVersion version,
                               boolean withBody) {
        this.status = status;
        this.keepAlive = keepAlive;
        this.version = version;
        this.withBody = withBody;
    }

    public Response buildResponse(String serverName) {
        String html = status.getIdAndMessage();
        return Response.ResponseBuilder.newInstance()
                .status(status)
                .server(serverName)
                .mediaType(MediaType.TEXT_HTML, null)
                .keepAlive(keepAlive, version)
                .contentLength(html.length())
                .body(withBody ? html.getBytes() : null)
                .build();
    }
}
