package com.webserver.exceptions;

public class WebProcessException extends RuntimeException {
    private final FailedResponseData failedResponseData;

    public WebProcessException(String errorMessage,
                               Throwable err,
                               FailedResponseData failedResponseData) {
        super(errorMessage, err);
        this.failedResponseData = failedResponseData;
    }

    public WebProcessException(String errorMessage,
                               FailedResponseData failedResponseData) {
        super(errorMessage, null);
        this.failedResponseData = failedResponseData;
    }

    public FailedResponseData failedResponse() {
        return failedResponseData;
    }
}
