# Introduction

A multi-threaded (e.g. file-based) web server with thread-pooling implemented in Java.

Plus: HTTP/1.1 keep-alive behavior to your implementation based on the http-client's <br>
capabilities exposed through its request headers.

# Solution

Please refer to [Solution.md](Solution.md) for the solution description and comments on the coding choices. 
