# Simple Web Server

## Introduction

'Simple web server' is a simple multi-threaded implementation of web server capabilities.
It serves the HEAD and GET HTTP requests with the static file resources.

The implementation follows a simple pattern:
1. The threads to serve requests are allocated from a fixed thread pool;
2. An open connection can be kept alive for some time; in this case the same thread is used
to serve several HTTP requests on the same connection. Otherwise a new thread is created on each
new incoming HTTP request;
3. The connection is established with the simple Java Sockets without 
usage of the Socket Channels.
4. The static resources, requested by HEAD/GET request, are found at the local directory,
and transmitted in the response body.

### Immutable objects

All used classes, even if not shared between threads, are designed as immutable. In fact,
only configuration and controller factory objects are shared between the threads. 

## 1. Configuration (com.webserver.config package)   

The configuration parameters are contained in **ServerConfig** and **FileServerConfig** classes
at the package ***com.webserver.config***.<br> 
At the given implementation the parameters are "hard-coded", but further they have to be 
(a) loaded from *.properties file, and
(b) can be modified with the environment variables.<br> 
It's important part for the further realization of CI - it will allow to launch the automatic 
tests with the parameters, defined through the environment variables. <br>

A single instance of the Configuration object is created, and that is the only object,
which is shared between the threads - nevertheless, it's designed as Immutable, and so
is available only for reading.

The configuration is a subject to simple validation process, where each parameter
is validated, and this validation is called before the launch of any server activity.
If the configuration is not valid, the user is informed, and required to launch the server
with a valid configuration.  

## 2. Web Server and Request Processor

### 2.1. Web Server (com.webserver.WebServer class)

WebServer creates the main thread pool and server socket, and launch a new thread on 
each new connection, instantiating a dedicated socket and submitting it to the thread.

The RequestProcessor class implements the Runnable interface, and implements
the given connection requests processing.  

### 2.2. Request Processor (com.webserver.RequestProcessor class) 

RequestProcessor maintains the keep-alive connection for the incoming requests until
the connection is closed or timed out.

It instantiates the RequestReader class and Response Writer class, that encapsulate
input and output streams of the connection first.

It instatiates a corresponding controller that is to serve the resources.  

The request is processed in three steps:
1. Reading the request;
2. Processing the request in order to find a requested data;
3. Building the response and sending it back to client. 

At the end it's checked if the connection has to be closed. If yes, the connection resources
are freed, and the thread is finished. If no, it waits for new incoming request.

Also the socket may issue a SocketTimeoutException to terminate the connection. 
If an Input/Output exception is thrown, the connection is closed and the thread is terminated.  

## 3. Request Reading (com.webserver.request package)

The **RequestReader** class reads from the input stream the request and
parses it. The **RequestReader** uses the **RequestBuilder** class to build 
the **Request** object.

The result of the *read()* operation is the **Request** object, that further
will pass to the controller to get the requested resources.

The resulting **Request** object consists of (a) **RequestLine**, (b) Headers, and
(c) Body if exists. **RequestLine** consists of method, uri, and protocol 
version.

The **Request** once formed, can submit itself to the processing 
by a Controller with the *processWith()* method. 

## 4. Controllers (com.webserver.request package)

Controller receives the **Request** object and returns the **Response** object.
Controller implements the ***IController*** interface with a single *processRequest()* method.

So any Controller class that implements this simple interface can 
serve to find a Response for a given Request.

**AbstractController** is a parent class for other controllers. **AbstractController**
analyzes the request Method and distributes it to the corresponding method,
which should be implemented at the derived class.   

The given package contains only 2 controller classes: **EchoController** and
**FileController**.

**EchoController** just returns the Response with a message about the asked 
resource.

**FileController** searches for the file requested and returns it if found.

The controllers are instantiated by **ControllerFactory**. Which type of
controller to be instantiated is defined at web server configuration.

**File Controller** returns the Response with the requested file. 
If the file is not precised, it searches for the default file at
the given directory (e.g. "index.html").<br>
If not found, it builds the NotFound Response;<br>
If media type is not supported, it builds Unsupported Media Type;<br>
If path is mal-formed, it returns the Bad Request response.

## 5. Response Writing (com.webserver.response package)   

The **Response** class contains the Status, Headers and Body. 
It also propagates the Keep-Alive flag from the request.
**ResponseBuilder** is used to build the Response object.

**ResponseWriter** writes the Response into output stream. It uses
the *BodyWriter* interface to write the response body. 
There are 2 implementations of *BodyWriter*:<br> 
**Response.DefaultBodyWriter** - to write from a byte array, and<br>
**FileController.FileBodyWriter** - to write from a file.   

## 6. Helpers (com.webserver.helpers package)

1. **FileExtension** enum enumerates the identifiable file extensions
mapping them to media types that should be returned in the response.

2. **HTTPHeader** contains the constants of the processed http headers.     

3. **MediaType** enum enumerates the media types.

4. **ProtocolVersion** enum enumerates the protocol versions.

## 7. Custom Exceptions (com.webserver.exceptions package)

**WebProcessException** is thrown in the process of resolving the request.<br>
**WebRequestException** is thrown when the request is considered as mal-formed.

**WebProcessException** and **WebRequestException** are raised when
the Response is considered to be possible to be returned to client.

Both are distingushed from IOException: in case of input/output error
the conenction is closed and no response is returned to client.  

## 8. Simple Logger (com.webserver.logging.SimpleLogger class)

A very basic logger, which service is to be extended for sure, but
provides 'info' and 'err' logging distinction, and the logged messages 
contains a thread identifier.

## 9. HTML templates (com.webserver.html.HtmlTemplate class)

The HtmlTemplate class contains the html message string template
to build the HTML responses. In an extended version these templates
should be loadable from as resources.

## Comments

This code is the subject to improvements. Also Unit Tests are to be added.
As well it should be load-tested.